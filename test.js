import http from 'k6/http';

export let options = {
    insecureSkipTLSVerify: true,
    noConnectionReuse: false,
    vus: 1000,
    duration: '1s',
};

export default () => {
    http.post(
        'http://localhost:3000/hello-world',
        JSON.stringify({
            title: 'message',
            content: {
                message: 'hello there',
                time: 'time',
            },
        }),
        {
            headers: {
                'Content-Type': 'application/json',
            },
        }
    );
};
