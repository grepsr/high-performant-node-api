import { log } from "../utils/logger";
import { getRemoteDetails } from "../utils/requests";
import snakecaseKeys from "snakecase-keys";
import { checkFloatingValue } from "../libs/converter";
import { FastifyReply, FastifyRequest } from "fastify";

interface RequestUrl {
    asin: string,
    store: string,
    url:string,
    include_html: boolean
}

export const getProductDetails = (client) => async (request: FastifyRequest, reply: FastifyReply) => {
    try {
        const { asin, store, url,include_html }: { asin: string, store: string, url: string, include_html: boolean } = request.query as RequestUrl;
        const message: Object = { asin, store, url,include_html };
        log(`Processing Asin: ${asin}`);
        const req_params = Object.assign(
            { type: "product" },
            message
        );
        const response = getRemoteDetails(message, client);
        //const questions_answered = getQuestionsAnsweredCount(message);
        Promise.all([response]).then((result: any) => {
            let [p_details] = result;
            if (p_details.status === true && p_details.message.response_code === 404) {
                reply.code(404).send({ success: false, message: "Product Not Found" });
            }
            if (p_details.status === true && p_details.message.status === true) {
                const req_details = Object.assign({},p_details.message.request_details);
                let response = manipulateResponse(p_details, include_html);
                reply.code(200).send({ success: true,request_parameters: req_params,request_details:req_details, content: response, response_ms: response.response_ms });
            }

            reply.code(503).send({ success: false, message: p_details.message });
        });
    } catch (error: any) {
        log(error, 'error');
        let data: Object = {
            success: false,
            error: error,
        };
        reply.code(400).send(data)
    }
};


const manipulateResponse = (info, include_html) => {
    //let response = snakecaseKeys(info.message);
    let response = info.message;
    // if ("price" in response.newer_version.price) {
    //     response.newer_version.price.price = checkFloatingValue(response.newer_version.price.price);
    //     response.newer_version.price.price_per_unit = checkFloatingValue(response.newer_version.price.price_per_unit);
    // }
    if ("rating_average" in response.ratings) {
        response.ratings.rating_average = checkFloatingValue(response.ratings.rating_average);
    }

    if(include_html != true && "html" in response){
        delete response.html;
    }

    if("request_details" in response){
        delete response.request_details;
    }

    // if ("product_price" in response) {
    //     response.product_price = checkFloatingValue(response.product_price);
    // }

    // if ("status" in qns && qns.status === true) {
    //     response['questions'] = { "questions_answered": qns.message.count };
    // }
    return response;
}

