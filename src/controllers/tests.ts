import axios from "axios";

export const testRequest = async (req, reply) => {
  let data: Object = {
    success: true,
    data: "Test Request",
  };
  reply.code(201).send(data);
};

export const ping = async (request, reply) => {
  return { pong: "It works!" };
};

export const appSearchKeywords = async (request, reply) => {
  axios({
    url: `https://api.gohelix.xyz/v1/amazon/search?search_term=${request.query.search_term}`,
    method: "GET",
    headers: {
      "Access-Control-Allow-Origin": "*",
      "x-api-key": "Ol9VWAMzFr5h4bXFiTfjb6Lqw14tQ7yuaLQ4WwL5",
    },
  })
    .then((resp) => {
      reply.code(201).send(resp.data);
    })
    .catch((err) =>
      reply
        .code(400)
        .json({ error: "Something went wrong", data: err.response.data })
    );
};


export const appSearchProducts = async (request, reply) => {
    axios({
      url: `https://api.gohelix.xyz/v1/amazon/product?search_term=${request.query.asin}`,
      method: "GET",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "x-api-key": "Ol9VWAMzFr5h4bXFiTfjb6Lqw14tQ7yuaLQ4WwL5",
      },
    })
      .then((resp) => {
        reply.code(201).send(resp.data);
      })
      .catch((err) =>
        reply
          .code(400)
          .json({ error: "Something went wrong", data: err.response.data })
      );
  };
