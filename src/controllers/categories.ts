import { log } from "../utils/logger";
import { getRemoteDetails } from "../utils/requests";
import { checkFloatingValue } from "../libs/converter";
import { FastifyReply, FastifyRequest } from "fastify";

interface RequestUrl {
  type: string;
  domain: string;
  parent_id: string;
}

export const getCategories =
  (client) => async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const {
        domain,
        parent_id,
        type,
      }: { type: string; domain: string; parent_id: string } =
        request.query as RequestUrl;
      const message: Object = { type, domain, parent_id };
      log(`Processing Categories Type: ${type}`);
      const response = sendRemoteRequest(message, client);
      Promise.all([response]).then((result: any) => {
        let [p_details] = result;
        if (
          p_details.status === true &&
          p_details.message.response_code === 404
        ) {
          reply
            .code(404)
            .send({ success: false, message: "Request Undefined" });
        }
        if (p_details.status === true && p_details.message.status === true) {
          reply.code(200).send({ success: true, categories: p_details.message.categories });
          reply.code(200).send(p_details);
        }

        reply.code(503).send({ success: false, message: p_details.message });
      });
    } catch (error: any) {
      log(error, "error");
      let data: Object = {
        success: false,
        error: error,
      };
      reply.code(400).send(data);
    }
  };

const sendRemoteRequest = async (req: Object, client) => {
  return new Promise((resolve, reject) => {
    client.get_categories(req, (err, response) => {
      if (err) {
        log(`Error Processing Request: ${err}`, "info");
        reject(err);
      }
      resolve({ status: true, message: response });
    });
  }).catch((err) => {
    return { status: false, message: err };
  });
};
