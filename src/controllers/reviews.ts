import { log } from "../utils/logger";
import { FastifyReply, FastifyRequest } from "fastify";

interface RequestUrl {
  asin: string;
  store: string;
  include_html: boolean;
}

export const getReviewsDetails =
  (client) => async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const message: Object = request.query as RequestUrl;
      // const message: Object = { asin }
      // log(`Processing Asin: ${asin}`);
      const req_params = Object.assign(
        { type: "reviews"},
        message
      );
      const filter_query = filterRequestQuery(message);
      const include_html = message["include_html"] || false;
      const response = sendRemoteRequest(filter_query, client);
      Promise.all([response]).then((result: any) => {
        let [p_details] = result;
        if (
          p_details.status === true &&
          p_details.message.responseCode === 404
        ) {
          reply
            .code(404)
            .send({ success: false, message: "Reviews Not Found" });
        }
        if (p_details.status === true && p_details.message.status === true) {
          const req_details = Object.assign({},p_details.message.request_details);
          let response = manipulateResponse(p_details, include_html);
          reply
            .code(200)
            .send({
              success: true,
              request_parameters: req_params,
              request_details:req_details,
              content: response,
              response_ms: response.response_ms,
            });
        }

        reply.code(503).send({ success: false, message: p_details.message });
      });
    } catch (error: any) {
      log(error, "error");
      let data: Object = {
        success: false,
        error: error,
      };
      reply.code(400).send(data);
    }
  };

const filterRequestQuery = (request: Object) => {
  if ("sort_by" in request) {
    let sort_name = getSortByValue(request["sort_by"]);
    request["sort_by"] = sort_name;
  }

  return request;
};



const getSortByValue = (value: string) => {
  let val: string;

  switch (value) {
    case "featured":
      val = "relevanceblender";
      break;
    case "price_low_to_high":
      val = "price-asc-rank";
      break;
    case "price_high_to_low":
      val = "price-desc-rank";
      break;
    case "most_recent":
      val = "date-desc-rank";
      break;
    case "average_review":
      val = "review-rank";
      break;
    default:
      val = "relevanceblender";
  }

  return val;
};

const sendRemoteRequest = async (req: Object, client) => {
  return new Promise((resolve, reject) => {
    client.get_product_reviews(req, (err, response) => {
      if (err) {
        log(`Error Processing Request: ${err}`, "info");
        reject(err);
      }
      resolve({ status: true, message: response });
    });
  }).catch((err) => {
    return { status: false, message: err };
  });
};

const manipulateResponse = (info, include_html) => {
  let response = info.message;
  try{

    if (include_html != true && "html" in response) {
      delete response.html;
    }
    
    if("request_details" in response){
        delete response.request_details;
    }

  } catch (err) {}
  return response;
};
