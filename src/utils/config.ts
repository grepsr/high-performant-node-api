import fs from 'fs';
import path from 'path';
import yaml from 'js-yaml';
import dot from 'dot-object';

const APP_ENV = process.env.APP_ENV || 'dev';
let doc = null;

try {
    let fileName = path.join(__dirname, `../../configs/${APP_ENV}.yaml`);
    if(APP_ENV == 'prd'){
        fileName = path.join(__dirname, `../../high-performant-node-api/configs/${APP_ENV}.yaml`);
    }
    doc = yaml.load(fs.readFileSync(fileName, 'utf8'));
} catch (e) {
    console.log('Could not load config');
    console.log(e);
    process.exit(1);
}

const getFromEnv = (key:String): any => {
    const envKey = `CFG_${key
    .split('.')
    .map(k => k.toUpperCase())
    .join('_')}`;
    return process.env[envKey];
};

export const cfg = (key: String, fallback:null|string = null): string => {
    // Check environment variable first
    let value: string = getFromEnv(key);
    if (!value) {
        value = dot.pick(key, doc) || fallback;
    }
    return value;
};