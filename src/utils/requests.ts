import { log } from "./logger";


export const getRemoteDetails = async (req: Object, client) => {
    return new Promise((resolve, reject) => {
        client.get_product_details(req, (err, response) => {
            if (err) {
                log(`Error Processing Request: ${err}`, 'info');
                reject(err);
            }
            resolve({ status: true, message: response });
        });
    }).catch(err => {
        return { status: false, message: err }
    });
}

// export const getQuestionsAnsweredCount = async(req: Object) => {
//     return new Promise((resolve, reject) => {
//         client.get_question_answers(req, (err, response) => {
//             if (err) {
//                 log(`Error Processing Request: ${err}`, 'info');
//                 reject(err);
//             }
//             resolve({ status: true, message: response });
//         });
//     }).catch(err => {
//         return { status: false, message: err }
//     });

// }