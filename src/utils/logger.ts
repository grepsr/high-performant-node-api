import loglevel from "loglevel";
const APP_ENV = process.env.APP_ENV || 'dev';

(APP_ENV !== 'dev') ? loglevel.setLevel("info"): loglevel.enableAll();

export const log = (message: string, level?: string) => {
    switch (level) {
        case "info":
            loglevel.info(message);
            break;
        case "warn":
            loglevel.warn(message);
            break;
        case "error":
            loglevel.error(message);
            break;
        case "debug":
            loglevel.debug(message);
            break;
        default:
            loglevel.log(message);
    }
}