import { RouteShorthandOptions } from "fastify";

export const AmazonOffers: RouteShorthandOptions = {
  schema: {
    summary:
      "The Offers API retrieves product offers results from an Amazon Store.",
    description:
      "The Offers API retrieves product offers results from an Amazon Store.",
    querystring: {
      type: "object",
      properties: {
        store: {
          type: "string",
          description:
            "Amazon domain to retrieve search results from for the search term provided. Supported list of Amazon domains can be found here.",
          style: "form",
          enum: ["US"],
        },
        asin: {
          type: "string",
          description:
            "Search term to be used to retrieve product listing. Please note that the requested term and actual term may vary as Amazon may correct typos to return nearest matches",
          style: "form",
          explode: true,
        },
        page: {
          type: "integer",
          description:
            "Specific page number to retrieve the results from. Returns items from the first page if left empty.",
          style: "form",
          explode: true,
          minimum: 1,
        },
        is_amazon_prime: {
          type: "boolean",
          description:
            "Determines the order in which search results are returned from amazon prime",
        },
        is_free_delivery: {
          type: "boolean",
          description:
            "Determines the order in which search results are returned from amazon prime",
        },
        is_new: {
          type: "boolean",
          description:
            "Determines the order in which search results are returned from amazon prime",
        },
        is_used: {
          type: "boolean",
          description:
            "Determines the order in which search results are returned from amazon prime",
        },
        is_used_like_new: {
          type: "boolean",
          description:
            "Determines the order in which search results are returned from amazon prime",
        },
        is_used_very_good: {
          type: "boolean",
          description:
            "Determines the order in which search results are returned from amazon prime",
        },
        is_used_good: {
          type: "boolean",
        },
        is_used_accesptable: {
          type: "boolean",
        },
        include_html: {
          type: "boolean",
          description: "Set true, If you want to get html. Defaults to false",
        },
      },
      required: ["asin"],
      // oneOf: [
      //   { required: ["asin"], not: { required: ["url"] } },
      //   { required: ["url"], not: { required: ["asin"] } },
      // ],
      errorMessage: {
        properties: {
          sort_by:
            "querystring.sort_by should be equal to one of the allowed values, Acceptable values are: featured, price_low_to_high, price_high_to_low, most_recent, average_review",
        },
      },
    },
    response: {
      200: {
        type: "object",
        description: "Product Details matching the parameters",
        properties: {
          success: {
            type: "boolean",
          },
          request_parameters: {
            type: "object",
            properties: {
              store: {
                type: "string",
                example: "US",
              },
              // domain: {
              //   type: "string",
              // },
              asin: {
                type: "string",
              },
              page: {
                type: "integer",
              },
              is_amazon_prime: {
                type: "boolean",
              },
              is_free_delivery: {
                type: "boolean",
              },
              is_new: {
                type: "boolean",
              },
              is_used: {
                type: "boolean",
              },
              is_used_like_new: {
                type: "boolean",
              },
              is_used_very_good: {
                type: "boolean",
              },
              is_used_good: {
                type: "boolean",
              },
              is_used_accesptable: {
                type: "boolean",
              },
              include_html: {
                type: "boolean",
              },
            },
          },
          request_details: {
            type: "object",
            properties: {
              requested_url: {
                type: "string",
              },
              canonical_url: {
                type: "string",
              },
              created_at: {
                type: "string",
              },
              response_ms: {
                type: "number",
              },
            },
          },
          content: { $ref: "#/definitions/message" },
        },
        definitions: {
          message: {
            type: "object",
            properties: {
              product: {
                $ref: "#/components/schemas/AmazonProductItem_product",
              },
              offers: {
                $ref: "#/components/schemas/AmazonProductItem_offers",
              },
              pagination: {
                $ref: "#/components/schemas/AmazonProductItem_pagination",
              },
              html: {
                type: "string",
                example: '"<!DOCTYPE html><html lang="en-us" .....',
              },
            },
          },
        },
        components: {
          schemas: {
            AmazonProductItem_offers: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  offer_id: {
                    type: "string",
                  },
                  flag_amazon_prime: {
                    type: "boolean",
                  },
                  is_buy_box_winner: {
                    type: "boolean",
                  },
                  condition: {
                    type: "string",
                  },
                  condition_text: {
                    type: "string",
                  },
                  price: {
                    $ref: "#/components/schemas/AmazonProductItem_price",
                  },
                  seller: {
                    $ref: "#/components/schemas/AmazonProductItem_seller_info",
                  },
                  fulfillment: {
                    $ref: "#/components/schemas/AmazonProductItem_seller_fullfillment",
                  },
                  ships_from: {
                    $ref: "#/components/schemas/AmazonProductItem_seller_ships_from",
                  },
                },
              },
            },
            AmazonProductItem_seller_ships_from: {
              type: "object",
              properties: {
                name: {
                  type: "string",
                },
                is_fba: {
                  type: "boolean",
                },
                url: {
                  type: "string",
                },
              },
            },
            AmazonProductItem_seller_fullfillment: {
              type: "object",
              properties: {
                standard_delivery: {
                  $ref: "#/components/schemas/AmazonProductItem_seller_fullfillment_primary_delivery",
                },
                fastest_delivery: {
                  $ref: "#/components/schemas/AmazonProductItem_seller_fullfillment_secondary_delivery",
                },
              },
            },
            AmazonProductItem_seller_fullfillment_primary_delivery: {
              type: "object",
              properties: {
                text: {
                  type: "string",
                  example: "FREE delivery Sunday January 9",
                },
                date_formatted: {
                  type: "string",
                  // "format": "date"
                },
              },
            },
            AmazonProductItem_seller_fullfillment_secondary_delivery: {
              type: "object",
              properties: {
                text: {
                  type: "string",
                  example:
                    "Or fastest delivery Friday January 7. Order within 19 hrs 33 mins",
                },
                date_formatted: {
                  type: "string",
                  // "format": "date"
                },
              },
            },
            AmazonProductItem_seller_info: {
              type: "object",
              properties: {
                name: {
                  type: "string",
                },
                url: {
                  type: "string",
                },
                seller_id: {
                  type: "string",
                },
                ratings: {
                  type: "object",
                  properties: {
                    ratings_average: {
                      type: "number",
                      example: 4.8,
                    },
                    ratings_total: {
                      type: "number",
                      example: 18498,
                    },
                    raw_text: {
                      type: "string",
                    },
                  },
                },
              },
            },
            AmazonProductItem_product: {
              type: "object",
              properties: {
                asin: {
                  type: "string",
                },
                title: {
                  type: "string",
                },
                url: {
                  type: "string",
                },
                image_url: {
                  type: "string",
                },
                rating_total: {
                  type: "integer",
                },
                rating_average: {
                  type: "number",
                },
              },
            },

            AmazonProductItem_pagination: {
              type: "object",
              properties: {
                current_page: {
                  type: "integer",
                },
                results_per_page: {
                  type: "integer",
                },
                available_pages: {
                  type: "integer",
                },
                results_total: {
                  type: "integer",
                },
              },
            },
            AmazonProductItem_price: {
              type: "object",
              properties: {
                currency_name: {
                  type: "string",
                  example: "USD",
                },
                currency_symbol: {
                  type: "string",
                  example: "$",
                },
                price: {
                  type: "number",
                  example: 53.11,
                },
                price_unit: {
                  type: "string",
                  example: "Count",
                },
                price_per_unit: {
                  type: "number",
                  example: 0.21,
                },
                raw_text: {
                  type: "string",
                  example: "$53.11 ($0.21 / Count)",
                },
              },
            },
          },
        },
      },
      400: {
        type: "object",
        properties: {
          success: {
            type: "boolean",
            example: false,
          },
          message: {
            type: "string",
          },
        },
      },
      503: {
        type: "object",
        properties: {
          success: {
            type: "boolean",
            example: false,
          },
          message: {
            type: "object",
            properties: {
              code: {
                type: "integer",
              },
              details: {
                type: "string",
              },
            },
          },
        },
      },
    },
  },
};
