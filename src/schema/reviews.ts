import { RouteShorthandOptions } from "fastify";

export const AmazonReviews: RouteShorthandOptions = {
  schema: {
    summary:
      "The Reviews API retrieves product reviews results from an Amazon Store.",
    description:
      "The Reviews API retrieves product reviews results from an Amazon Store.",
    querystring: {
      type: "object",
      properties: {
        store: {
          type: "string",
          description:
            "Amazon domain to retrieve search results from for the search term provided. Supported list of Amazon domains can be found here.",
          style: "form",
          enum: ["US"],
        },
        asin: {
          type: "string",
          description:
            "Search term to be used to retrieve product listing. Please note that the requested term and actual term may vary as Amazon may correct typos to return nearest matches",
          style: "form",
          explode: true,
          minLength: 10,
          maxLength: 10,
          pattern: "(^[A-Z0-9]{10}$)",
        },
        page: {
          type: "integer",
          description:
            "Specific page number to retrieve the results from. Returns items from the first page if left empty.",
          style: "form",
          explode: true,
          minimum: 1,
        },
        sort_by: {
          type: "string",
          style: "form",
          explode: true,
          enum: ["recent", "helpful"],
          description:
            "Determines the order in which search results are returned from reviews. Acceptable values are: featured, recent, helpful",
        },
        reviewer_type: {
          type: "string",
          style: "form",
          explode: true,
          enum: ["avp_only_reviews", "all_reviews"],
          description:
            "Determines the order in which search results are returned from verified reviews. Acceptable values are: avp_only_reviews, all_reviews",
        },
        filter_by_star: {
          type: "string",
          style: "form",
          explode: true,
          enum: [
            "five_star",
            "four_star",
            "three_star",
            "two_star",
            "one_star",
            "all_stars",
            "positive",
            "critical"
          ],
          description:
            "Determines the order in which reviews results are returned filtered by ratings stars. Acceptable values are: five_star, four_star,three_star,two_star,one_star,all_stars",
        },
        media_type:{
          type: "string",
          style: "form",
          explode: true,
          enum: [
            "media_reviews_only",
            "all_content",
          ],
          description:
            "Determines the order in which reviews results are returned filtered by ratings stars. Acceptable values are: media_reviews_only, all_content",
        },
        format_type:{
          type: "string",
          style: "form",
          explode: true,
          enum: [
            "current_format",
            "all_formats",
          ],
          description:
            "Determines the order in which reviews results are returned filtered by formats. Acceptable values are: current_format, all_formats",

        },
        // url: {
        //   type: "string",
        //   format: "url",
        //   description: "URL of the search page to be retrieved.",
        // },
        include_html: {
          type: "boolean",
          description: "Set true, If you want to get html. Defaults to false",
        },
      },
      required: ["asin"],
      // oneOf: [
      //   { required: ["asin"], not: { required: ["url"] } },
      //   { required: ["url"], not: { required: ["asin"] } },
      // ],
      errorMessage: {
        properties: {
          sort_by:
            "querystring.sort_by should be equal to one of the allowed values, Acceptable values are: featured, price_low_to_high, price_high_to_low, most_recent, average_review",
        },
      },
    },
    response: {
      200: {
        type: "object",
        description: "Product Details matching the parameters",
        properties: {
          success: {
            type: "boolean",
          },
          request_parameters: {
            type: "object",
            properties: {
              store: {
                type: "string",
                example: "US",
              },
              // domain: {
              //   type: "string",
              // },
              asin: {
                type: "string",
              },
              page: {
                type: "integer",
              },
              category_id: {
                type: "string",
              },
              sort_by: {
                type: "string",
              },
              url: {
                type: "string",
              },
              include_html: {
                type: "boolean",
              },
            },
          },
          request_details: {
            type: "object",
            properties: {
              requested_url: {
                type: "string",
              },
              canonical_url: {
                type: "string",
              },
              created_at: {
                type: "string",
              },
              response_ms: {
                type: "number",
              },
            },
          },
          content: { $ref: "#/definitions/message" },
        },
        definitions: {
          message: {
            type: "object",
            properties: {
              ratings: {
                $ref: "#/components/schemas/AmazonProductItem_ratings",
              },
              product: {
                $ref: "#/components/schemas/AmazonProductItem_product",
              },
              reviews: {
                $ref: "#/components/schemas/AmazonProductItem_reviews",
              },
              pagination: {
                $ref: "#/components/schemas/AmazonProductItem_pagination",
              },
            },
          },
        },
        components: {
          schemas: {
            AmazonProductItem_variant: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  name: {
                    type: "string",
                  },
                  value: {
                    type: "string",
                  },
                },
              },
            },
            AmazonProductItem_reviews: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  reviews_country: {
                    type: "string",
                  },
                  rank_absolute: {
                    type: "number",
                  },
                  rank_relative_to_page: {
                    type: "number",
                  },
                  is_verified_purchase: {
                    type: "boolean",
                  },
                  is_vine_program: {
                    type: "boolean",
                  },
                  is_global_review: {
                    type: "boolean",
                  },
                  rating: {
                    type: "number",
                  },
                  review_id: {
                    type: "string",
                  },
                  review_title: {
                    type: "string",
                  },
                  review_text: {
                    type: "string",
                  },
                  helpful_count: {
                    type: "number",
                  },
                  review_date: {
                    type: "object",
                    properties: {
                      raw_text: {
                        type: "string",
                      },
                      formatted_date: {
                        type: "string",
                      },
                    },
                  },
                  variant: {
                    $ref: "#/components/schemas/AmazonProductItem_variant",
                  },
                  reviewer: {
                    $ref: "#/components/schemas/AmazonProductItem_reviewer",
                  },
                  images: {
                    $ref: "#/components/schemas/AmazonProductItem_images",
                  },
                  videos: {
                    $ref: "#/components/schemas/AmazonProductItem_videos",
                  },
                },
              },
            },
            AmazonProductItem_product: {
              type: "object",
              properties: {
                asin: {
                  type: "string",
                },
                title: {
                  type: "string",
                },
                image_url: {
                  type: "string",
                },
                variant: {
                  $ref: "#/components/schemas/AmazonProductItem_variant",
                },
                brand: {
                  type: "object",
                  properties: {
                    name: {
                      type: "string",
                    },
                    url: {
                      type: "string",
                    },
                  },
                },
              },
            },
            AmazonProductItem_ratings: {
              type: "object",
              properties: {
                ratings_average: {
                  type: "number",
                  example: 4.8,
                },
                ratings_total: {
                  type: "number",
                  example: 18498,
                },
                ratings_filtered_total: {
                  type: "number",
                  example: 184,
                },
                reviews_filtered_total: {
                  type: "number",
                },
                breakdown: {
                  $ref: "#/components/schemas/AmazonProductItem_ratings_breakdown",
                },
              },
            },
            AmazonProductItem_ratings_breakdown: {
              type: "array",
              items: {
                types: "object",
                properties: {
                  stars: {
                    type: "number",
                    example: 5,
                  },
                  percentage: {
                    type: "number",
                    example: 88,
                  },
                  count: {
                    type: "number",
                    example: 16278,
                  },
                },
              },
            },
            AmazonProductItem_images: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  link: {
                    type: "string",
                  },
                },
              },
            },
            AmazonProductItem_videos: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  link: {
                    type: "string",
                  },
                },
              },
            },
            AmazonProductItem_reviewer: {
              type: "object",
              properties: {
                name: {
                  type: "string",
                },
                profile_url: {
                  type: "string",
                },
                id: {
                  type: "string",
                },
                image_url: {
                  type: "string",
                },
              },
            },
            AmazonProductItem_pagination: {
              type: "object",
              properties: {
                reviews_filtered_total: {
                  type: "number",
                },
                current_page: {
                  type: "number",
                },
                results_per_page: {
                  type: "number",
                },
                available_pages: {
                  type: "number",
                },
                next_page_url: {
                  type: "string",
                },
              },
            },
            AmazonProductItem_price: {
              type: "object",
              properties: {
                currency_name: {
                  type: "string",
                  example: "USD",
                },
                currency_symbol: {
                  type: "string",
                  example: "$",
                },
                price: {
                  type: "number",
                  example: 53.11,
                },
                price_unit: {
                  type: "string",
                  example: "Count",
                },
                price_per_unit: {
                  type: "number",
                  example: 0.21,
                },
                raw_text: {
                  type: "string",
                  example: "$53.11 ($0.21 / Count)",
                },
              },
            },
          },
        },
      },
      400: {
        type: "object",
        properties: {
          success: {
            type: "boolean",
            example: false,
          },
          message: {
            type: "string",
          },
        },
      },
      503: {
        type: "object",
        properties: {
          success: {
            type: "boolean",
            example: false,
          },
          message: {
            type: "object",
            properties: {
              code: {
                type: "integer",
              },
              details: {
                type: "string",
              },
            },
          },
        },
      },
    },
  },
};
