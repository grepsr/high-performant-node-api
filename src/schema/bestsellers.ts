import { RouteShorthandOptions } from "fastify";

export const AmazonBestSellers: RouteShorthandOptions = {
  schema: {
    summary:
      "The Reviews API retrieves product reviews results from an Amazon Store.",
    description:
      "The Reviews API retrieves product reviews results from an Amazon Store.",
    querystring: {
      type: "object",
      properties: {
        store: {
          type: "string",
          description:
            "Amazon domain to retrieve search results from for the search term provided. Supported list of Amazon domains can be found here.",
          style: "form",
          enum: ["US"],
        },
        category_id: {
          type: "string",
          description:
            "Provide a category_id if you want to limit the search results to a specific category only. If left empty, the API will search results across all categories.",
          style: "form",
          explode: true,
        },
        page: {
          type: "integer",
          description:
            "Specific page number to retrieve the results from. Returns items from the first page if left empty.",
          style: "form",
          explode: true,
          minimum: 1,
        },
        url: {
          type: "string",
          format: "url",
          description: "URL of the search page to be retrieved.",
        },
        include_html: {
          type: "boolean",
          description: "Set true, If you want to get html. Defaults to false",
        },
      },
      required: ["category_id"],
      oneOf: [
        { required: ["category_id"], not: { required: ["url"] } },
        { required: ["url"], not: { required: ["category_id"] } },
      ],
      errorMessage: {
        properties: {
          sort_by:
            "querystring.sort_by should be equal to one of the allowed values, Acceptable values are: featured, price_low_to_high, price_high_to_low, most_recent, average_review",
        },
      },
    },
    response: {
      200: {
        type: "object",
        description: "Product Details matching the parameters",
        properties: {
          success: {
            type: "boolean",
          },
          request_parameters: {
            type: "object",
            properties: {
              store: {
                type: "string",
                example: "US",
              },
              page: {
                type: "integer",
              },
              category_id: {
                type: "string",
              },
              url: {
                type: "string",
              },
              include_html: {
                type: "boolean",
              },
            },
          },
          request_details: {
            type: "object",
            properties: {
              requested_url: {
                type: "string",
              },
              canonical_url: {
                type: "string",
              },
              created_at: {
                type: "string",
              },
              response_ms: {
                type: "number",
              },
            },
          },
          content: { $ref: "#/definitions/message" },
        },
        definitions: {
          message: {
            type: "object",
            properties: {
              bestsellers: {
                $ref: "#/components/schemas/AmazonProductItem_best",
              },
              pagination: {
                $ref: "#/components/schemas/AmazonProductItem_pagination",
              },
            },
          },
        },
        components: {
          schemas: {
            AmazonProductItem_best: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  rank_absolute: {
                    type: "number",
                  },
                  rank_relative_to_page: {
                    type: "number",
                  },
                  asin: {
                    type: "string",
                  },
                  product_title: {
                    type: "string",
                  },
                  image_url: {
                    type: "string",
                  },
                  product_url: {
                    type: "string",
                  },
                  ratings_average: {
                    type: "number",
                    example: 4.8,
                  },
                  ratings_total: {
                    type: "number",
                    example: 18498,
                  },
                  price: {
                    $ref: "#/components/schemas/AmazonProductItem_price",
                  },
                },
              },
            },

            AmazonProductItem_pagination: {
              type: "object",
              properties: {
                current_page: {
                  type: "number",
                },
                results_per_page: {
                  type: "number",
                },
                available_pages: {
                  type: "number",
                },
                next_page_url: {
                  type: "string",
                },
              },
            },
            AmazonProductItem_price: {
              type: "object",
              properties: {
                currency_name: {
                  type: "string",
                  example: "USD",
                },
                currency_symbol: {
                  type: "string",
                  example: "$",
                },
                price: {
                  type: "number",
                  example: 53.11,
                },
                lowest_price: {
                  type: "number",
                  example: 53.11,
                },
                highest_price: {
                  type: "number",
                  example: 503.11,
                },
                
                raw_text: {
                  type: "string",
                  example: "$53.11 ($0.21 / Count)",
                },
              },
            },
          },
        },
      },
      400: {
        type: "object",
        properties: {
          success: {
            type: "boolean",
            example: false,
          },
          message: {
            type: "string",
          },
        },
      },
      503: {
        type: "object",
        properties: {
          success: {
            type: "boolean",
            example: false,
          },
          message: {
            type: "object",
            properties: {
              code: {
                type: "integer",
              },
              details: {
                type: "string",
              },
            },
          },
        },
      },
    },
  },
};
