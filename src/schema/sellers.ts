import { RouteShorthandOptions } from "fastify";

export const AmazonSellers: RouteShorthandOptions = {
  schema: {
    summary:
      "The Seller API is used to pull information about the seller of a particular product. The seller page contains the details about the seller’s Amazon store such as the store name, ratings, name of the business and its contact information. ",
    description:
      "The Seller API is used to pull information about the seller of a particular product. The seller page contains the details about the seller’s Amazon store such as the store name, ratings, name of the business and its contact information. ",
    querystring: {
      type: "object",
      properties: {
        store: {
          type: "string",
          description:
            "determines which store to request data from. Defaults to the US store if left empty. ",
          style: "form",
          enum: ["US"],
        },
        seller_id: {
          type: "string",
          description: "This is the Amazon issued ID for the seller’s store",
          style: "form",
          explode: true,
        },
        include_html: {
          type: "boolean",
          description: "Set true, If you want to get html. Defaults to false",
        },
      },
      required: ["seller_id"],
      // oneOf: [
      //   { required: ["asin"], not: { required: ["url"] } },
      //   { required: ["url"], not: { required: ["asin"] } },
      // ],
      errorMessage: {
        properties: {
          sort_by:
            "querystring.sort_by should be equal to one of the allowed values, Acceptable values are: featured, price_low_to_high, price_high_to_low, most_recent, average_review",
        },
      },
    },
    response: {
      200: {
        type: "object",
        description: "Product Details matching the parameters",
        properties: {
          success: {
            type: "boolean",
          },
          request_parameters: {
            type: "object",
            properties: {
              store: {
                type: "string",
                example: "US",
              },
              seller_id: {
                type: "string",
              },
              include_html: {
                type: "boolean",
              },
            },
          },
          request_details: {
            type: "object",
            properties: {
              requested_url: {
                type: "string",
              },
              canonical_url: {
                type: "string",
              },
              created_at: {
                type: "string",
              },
              response_ms: {
                type: "number",
              },
            },
          },
          content: { $ref: "#/definitions/message" },
        },
        definitions: {
          message: {
            type: "object",
            properties: {
              seller: {
                $ref: "#/components/schemas/AmazonSeller",
              },
              ratings: {
                $ref: "#/components/schemas/AmazonProductItem_ratings",
              },
              recent_feedback: {
                $ref: "#/components/schemas/AmazonRecent_feedback",
              },
              html: {
                type: "string",
                example: '"<!DOCTYPE html><html lang="en-us" .....',
              },
            },
          },
        },
        components: {
          schemas: {
            AmazonProductItem_ratings: {
              type: "object",
              properties: {
                total_ratings: {
                  type: "number",
                },
                rating: {
                  type: "number",
                },
                raw_text: {
                  type: "string",
                },
                thirty_days: {
                  $ref: "#/components/schemas/AmazonProductItem_ratings_info",
                },
                ninety_days: {
                  $ref: "#/components/schemas/AmazonProductItem_ratings_info",
                },
                twelve_months: {
                  $ref: "#/components/schemas/AmazonProductItem_ratings_info",
                },
                lifetime: {
                  $ref: "#/components/schemas/AmazonProductItem_ratings_info",
                },
              },
            },
            AmazonProductItem_ratings_info: {
              type: "object",
              properties: {
                count: {
                  type: "number",
                },
                percentage_positive_ratings: {
                  type: "string",
                },
                percentage_neutral_ratings: {
                  type: "string",
                },
                percentage_negative_ratings: {
                  type: "string",
                },
              },
            },
            AmazonSeller: {
              type: "object",
              properties: {
                seller_id: {
                  type: "string",
                },
                store_name: {
                  type: "string",
                },
                url: {
                  type: "string",
                },
                logo: {
                  type: "string",
                },
                byline: {
                  type: "string",
                },
                storefront_url: {
                  type: "string",
                },
                business_name: {
                  type: "string",
                },
                phone: {
                  type: "string",
                },
                full_address: {
                  type: "string",
                },
                address_rows: {
                  type: "array",
                  items: {
                    type: "string",
                  },
                },
              },
            },
            AmazonRecent_feedback: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  rating: {
                    type: "number",
                  },
                  text: {
                    type: "string",
                  },
                  reviewer: {
                    type: "string",
                  },
                  formatted_date: {
                    type: "string",
                  },
                },
              },
            },
          },
        },
      },
      400: {
        type: "object",
        properties: {
          success: {
            type: "boolean",
            example: false,
          },
          message: {
            type: "string",
          },
        },
      },
      503: {
        type: "object",
        properties: {
          success: {
            type: "boolean",
            example: false,
          },
          message: {
            type: "object",
            properties: {
              code: {
                type: "integer",
              },
              details: {
                type: "string",
              },
            },
          },
        },
      },
    },
  },
};
