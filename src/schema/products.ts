import { RouteShorthandOptions } from "fastify";

export const AmazonProducts: RouteShorthandOptions = {
  schema: {
    summary: "Gets Product Data from Amazon",
    description:
      "By passing in the appropriate options, you can request for product details for products in different Amazon markets.\n",
    querystring: {
      type: "object",
      properties: {
        store: {
          type: "string",
          description:
            "country for the product lookup, defines the Amazon domain to be used.",
          style: "form",
          enum: ["US"],
        },
        asin: {
          type: "string",
          description: "ASIN of the Amazon Product to be fetched",
          style: "form",
          explode: true,
          minLength: 10,
          maxLength: 10,
          pattern: "(^[A-Z0-9]{10}$)",
        },
        url: {
          type: "string",
          format: "uri",
          pattern: "amazon.com/?.*/dp/[A-Z0-9]{10}",
        },
        include_html: {
          type: "boolean",
          description: "Set true, If you want to get html. Defaults to false",
        },
      },
      oneOf: [
        { required: ["asin"], not: { required: ["url"] } },
        { required: ["url"], not: { required: ["asin"] } },
      ],
    },
    response: {
      200: {
        type: "object",
        description: "Product Details matching the parameters",
        properties: {
          success: {
            type: "boolean",
          },
          request_parameters: {
            type: "object",
            properties: {
              store: {
                type: "string",
                example: "US",
              },
              // domain: {
              //   type: "string",
              // },
              asin: {
                type: "string",
              },
              url: {
                type: "string",
              },
              include_html: {
                type: "boolean",
              },
            },
          },
          request_details: {
            type: "object",
            properties: {
              requested_url: {
                type: "string",
              },
              canonical_url: {
                type: "string",
              },
              created_at: {
                type: "string",
              },
              response_ms: {
                type: "number",
              },
            },
          },
          content: { $ref: "#/definitions/message" },
        },
        definitions: {
          message: {
            type: "object",
            properties: {
              asin: {
                type: "string",
                example: "B00TJ6WLV2",
              },
              asin_redirected: {
                type: "string",
                format: "nullable",
              },
              //   canonical_url: {
              //     type: "string",
              //     example:
              //       "https://www.amazon.com/Pampers-Baby-Dry-Disposable-Diapers-Count/dp/B00TJ6WLV2",
              //   },
              product_title: {
                type: "string",
                example:
                  "Diapers Newborn/Size 1 (8-14 lb) 120 Count - Pampers Baby Dry Disposable Baby Diapers Super Pack",
              },
              brand: {
                $ref: "#/components/schemas/AmazonProductItem_brand",
              },
              search_alias: {
                $ref: "#/components/schemas/AmazonProductItem_search_alias",
              },
              categories: {
                $ref: "#/components/schemas/AmazonProductItem_category",
              },
              variants: {
                $ref: "#/components/schemas/AmazonProductItem_variant",
              },
              images: {
                $ref: "#/components/schemas/AmazonProductItem_images",
              },
              videos: {
                $ref: "#/components/schemas/AmazonProductItem_videos",
              },
              product_overview_atf: {
                $ref: "#/components/schemas/AmazonProductItem_overview_atf",
              },
              feature_bullets: {
                $ref: "#/components/schemas/AmazonProductItem_feature_bullets",
              },
              newer_version: {
                $ref: "#/components/schemas/AmazonProductItem_newer_version",
              },

              ratings: {
                $ref: "#/components/schemas/AmazonProductItem_ratings",
              },
              questions: {
                $ref: "#/components/schemas/AmazonProductItem_questions",
              },
              amazon_choice: {
                $ref: "#/components/schemas/AmazonProductItem_amazon_choice",
              },
              
              offers: {
                $ref: "#/components/schemas/AmazonProductItem_offers",
              },
              frequently_bought_together_asins: {
                $ref: "#/components/schemas/AmazonProductItem_frequently_bought_together_asins",
              },
              buy_box_winner: {
                $ref: "#/components/schemas/AmazonProductItem_buy_box_winner",
              },
              availability: {
                type: "object",
                properties: {
                  text: {
                    type: "string",
                  },
                  raw_text: {
                    type: "string",
                  },
                },
              },
              add_a_protection_plan: {
                $ref: "#/components/schemas/AmazonProductItem_buy_box_winner_accessory_plan_protection",
              },
              add_an_accessory: {
                $ref: "#/components/schemas/AmazonProductItem_buy_box_winner_accessory_plan_protection",
              },
              special_offers_and_promotions: {
                type: "array",
                items: {
                  type: "string",
                  example:
                    "Your cost could be $$0.00 instead of $$26.94! Get a $50 Amazon Gift Card instantly upon approval for the Amazon Rewards Visa Card Apply now",
                },
              },
              aplus_content: {
                $ref: "#/components/schemas/AmazonProductItem_aplus_content",
              },
              documents: {
                type: "array",
                items: {
                  type: "object",
                  properties: {
                    title: {
                      type: "string",
                    },
                    url: {
                      type: "string",
                    },
                  },
                },
              },
              product_description: {
                type: "string",
                example:
                  "Designed to help keep skin dry and healthy Pampers Baby-Dry diapers feature LockAway Channels to absorb wetness and lock it away from skin for up to 12 hours of dryness. Plus new and improved Dual Leak-Guard Barriers (sizes 1-6) help protect where leaks happen most for all-night sleep protection. Pampers Wetness Indicator changes color when baby is wet so you know when it’s time for a change. Gentle on baby’s skin Pampers Baby-Dry diapers are hypoallergenic and free of parabens and latex (natural rubber) and are Skin Health Alliance dermatologist approved. For trusted protection trust Pampers the #1 pediatrician recommended brand.",
              },
              product_details: {
                $ref: "#/components/schemas/AmazonProductItem_product_details",
              },
              best_seller_ranks: {
                $ref: "#/components/schemas/AmazonProductItem_best_seller_ranks",
              },
              important_information: {
                $ref: "#/components/schemas/AmazonProductItem_information",
              },
              
              first_available: {
                type: "string",
                example: "February 12, 2015",
              },
              manufacturer: {
                type: "string",
                example: "Procter & Gamble",
              },
              country_of_origin: {
                type: "string",
                example: "USA",
              },
              html: {
                type: "string",
                example: '"<!DOCTYPE html><html lang="en-us" .....',
              },
            },
            required: ["asin", "brand", "categories", "product_title"],
          },
        },
        components: {
          schemas: {
            AmazonProductItem_offers: {
              type: "object",
              properties: {
                count: {
                  type: "number",
                  example: 5,
                },
                price: {
                  $ref: "#/components/schemas/AmazonProductItem_newer_version_price",
                },
                url: {
                  type: "string",
                  example:
                    "https://www.amazon.com/gp/offer-listing/B00TJ6WLV2/ref=dp_olp_NEW_mbc?ie=UTF8&condition=NEW",
                },
              },
            },
            AmazonProductItem_buy_box_winner: {
              type: "object",
              properties: {
                price: {
                  $ref: "#/components/schemas/AmazonProductItem_newer_version_price",
                },
                availability: {
                  $ref: "#/components/schemas/AmazonProductItem_buy_box_winner_availability",
                },
                offer_id: {
                  type: "string",
                  example:
                    "+yUqcwZ792AZjbwmNUX6jcnKolX0fYw1wAPIGkgGOz0BuZwTs22VQ/QK1dNdSzcvY2mb7BOChrgjdcrbXx73yDgVs9MHQAmoP2zODtAJ6FYS6CwN3lFiuqm2SQXz+n4A/67zNj/TrW+pc68ujuQ+iw==",
                },
                seller: {
                  $ref: "#/components/schemas/AmazonProductItem_buy_box_winner_seller",
                },
                fulfillment: {
                  $ref: "#/components/schemas/AmazonProductItem_buy_box_winner_fulfillment",
                },
                ships_from: {
                  $ref: "#/components/schemas/AmazonProductItem_buy_box_winner_ships_from",
                },
              },
            },
            AmazonProductItem_buy_box_winner_availability: {
              type: "object",
              properties: {
                text: {
                  type: "string",
                  example: "Out of Stock",
                },
                raw_text: {
                  type: "string",
                  example:
                    "Currently unavailable. We don't know when or if this item will be back in stock.",
                },
              },
            },
            AmazonProductItem_buy_box_winner_seller: {
              type: "object",
              properties: {
                name: {
                  type: "string",
                  example: "Amazon.com",
                },
                url: {
                  type: "string",
                  format: "nullable",
                },
                seller_id: {
                  type: "string",
                  format: "nullable",
                },
              },
            },
            AmazonProductItem_buy_box_winner_fulfillment_primary_delivery: {
              type: "object",
              properties: {
                text: {
                  type: "string",
                  example: "FREE delivery Sunday January 9",
                },
                date_formatted: {
                  type: "string",
                  // "format": "date"
                },
              },
            },
            AmazonProductItem_buy_box_winner_fulfillment_secondary_delivery: {
              type: "object",
              properties: {
                text: {
                  type: "string",
                  example:
                    "Or fastest delivery Friday January 7. Order within 19 hrs 33 mins",
                },
                date_formatted: {
                  type: "string",
                  // "format": "date"
                },
              },
            },
            AmazonProductItem_buy_box_winner_fulfillment: {
              type: "object",
              properties: {
                standard_delivery: {
                  $ref: "#/components/schemas/AmazonProductItem_buy_box_winner_fulfillment_primary_delivery",
                },
                fastest_delivery: {
                  $ref: "#/components/schemas/AmazonProductItem_buy_box_winner_fulfillment_secondary_delivery",
                },
              },
            },
            AmazonProductItem_buy_box_winner_ships_from: {
              type: "object",
              properties: {
                name: {
                  type: "string",
                  example: "Amazon.com",
                },
                is_fba: {
                  type: "boolean",
                },
              },
            },
            AmazonProductItem_brand: {
              type: "object",
              properties: {
                name: {
                  type: "string",
                  example: "Pampers",
                },
                url: {
                  type: "string",
                  pattern: "amazon.com",
                  format: "uri",
                  example:
                    "https://www.amazon.com/stores/Pampers/page/EEA5A814-FADA-4D6A-A235-CC4D30B5553F?ref_=ast_bln",
                },
              },
            },
            AmazonProductItem_search_alias: {
              type: "object",
              properties: {
                title: {
                  type: "string",
                  example: "Health, Household & Baby Care",
                },
                id: {
                  type: "string",
                  example: "hpc",
                },
              },
            },
            AmazonProductItem_questions: {
              type: "object",
              properties: {
                questions_answered: {
                  type: "number",
                  example: 42,
                },
              },
            },
            AmazonProductItem_category: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  title: {
                    type: "string",
                    example: "Health Household & Baby Care",
                  },
                  node_id: {
                    type: "number",
                    example: 16041231233,
                  },
                  url: {
                    type: "string",
                    example:
                      "https://www.amazon.com/baby-car-seats-strollers-bedding/b/ref=dp_bc_aui_C_1/147-7978996-3262120?ie=UTF8&node=165796011",
                  },
                },
              },
            },
            AmazonProductItem_buy_box_winner_accessory_plan_protection: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  asin: {
                    type: "string",
                  },
                  title: {
                    type: "string",
                  },
                  price: {
                    $ref: "#/components/schemas/AmazonProductItem_no_unit_price",
                  },
                },
              },
            },
            AmazonProductItem_images: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  variant: {
                    type: "string",
                    example: "MAIN",
                  },
                  link: {
                    type: "string",
                    example:
                      "https://m.media-amazon.com/images/I/617NtexaW2L._AC_SL1500_.jpg",
                  },
                },
              },
            },
            AmazonProductItem_videos: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  variant: {
                    type: "string",
                    example: "MAIN",
                  },
                  language_code: {
                    type: "string",
                    example: "en_US",
                  },
                  is_hero_video: {
                    type: "boolean",
                    example: true,
                  },
                  video_height: {
                    type: "integer",
                    example: 480,
                  },
                  video_width: {
                    type: "integer",
                    example: 800,
                  },
                  duration_seconds: {
                    type: "integer",
                    example: 34,
                  },
                  title: {
                    type: "string",
                    example: "SanDisk Ultra microSD Memory Card",
                  },
                  thumb: {
                    type: "string",
                    example:
                      "https://m.media-amazon.com/images/I/51hoTKyxlxL.SS40_BG85,85,85_BR-120_PKdp-play-icon-overlay__.jpg",
                  },
                  url: {
                    type: "string",
                    example:
                      "https://m.media-amazon.com/images/S/vse-vms-transcoding-artifact-us-east-1-prod/5c1181fe-7c83-4436-a466-0021e84ef18e/default.jobtemplate.mp4.480.mp4",
                  },
                },
              },
            },
            AmazonProductItem_overview_atf: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  label: {
                    type: "string",
                    example: "Size",
                  },
                  value: {
                    type: "string",
                    example: "Size 1 (120 Count)",
                  },
                },
              },
            },
            AmazonProductItem_newer_version: {
              type: "object",
              properties: {
                product_title: {
                  type: "string",
                  example:
                    "Diapers Newborn/Size 1 (8-14 lb) 252 Count - Pampers Baby Dry Disposable Baby Diapers ONE MONTH SUPPLY",
                },
                product_image_url: {
                  type: "string",
                  example:
                    "https://m.media-amazon.com/images/I/71oUZyIeZPS._SR7575_.jpg",
                },
                url: {
                  type: "string",
                  example:
                    "https://www.amazon.com/Diapers-Newborn-Size-8-14-Count-dp-B07H38BPJX/dp/B07H38BPJX/ref=dp_ob_image_hpc",
                },
                asin: {
                  type: "string",
                  example: "B07H38BPJX",
                },
                price: {
                  $ref: "#/components/schemas/AmazonProductItem_newer_version_price",
                },
              },
            },
            AmazonProductItem_variant: {
              type: "object",
              properties: {
                display_type: {
                  type: "string",
                  example: "swatch",
                },
                variant_products: {
                  type: "array",
                  items: {
                    type: "object",
                    properties: {
                      asin: {
                        type: "string",
                        example: "B00DEYYRGM",
                      },
                      variant_title: {
                        type: "string",
                        example: "click to select Size 0",
                      },
                      is_current_product: {
                        type: "boolean",
                        example: true,
                      },
                      product_url: {
                        type: "string",
                        example:
                          "https://www.amazon.com/Simple-Joys-Carters-4-Pack-Months/dp/B01MDUHP50",
                      },
                      price: {
                        $ref: "#/components/schemas/AmazonProductItem_newer_version_price",
                      },
                      variant_attributes: {
                        type: "array",
                        items: {
                          type: "object",
                          properties: {
                            attribute_name_display_label: {
                              type: "string",
                              example: "size",
                            },
                            attribute_name: {
                              type: "string",
                              example: "size_name",
                            },
                            attribute_value_display_label: {
                              type: "string",
                              example: "Size 4 (92 Count)",
                            },
                            attribute_value: {
                              type: "string",
                              example: "3",
                            },
                          },
                        },
                      },
                      images: {
                        $ref: "#/components/schemas/AmazonProductItem_images",
                      },
                    },
                  },
                },
              },
            },
            AmazonProductItem_newer_version_price: {
              type: "object",
              properties: {
                currency_name: {
                  type: "string",
                  example: "USD",
                },
                currency_symbol: {
                  type: "string",
                  example: "$",
                },
                price: {
                  type: "number",
                  example: 53.11,
                },
                price_unit: {
                  type: "string",
                  example: "Count",
                },
                price_per_unit: {
                  type: "number",
                  example: 0.21,
                },
                raw_text: {
                  type: "string",
                  example: "$53.11 ($0.21 / Count)",
                },
              },
            },
            AmazonProductItem_no_unit_price: {
              type: "object",
              properties: {
                currency_name: {
                  type: "string",
                  example: "USD",
                },
                currency_symbol: {
                  type: "string",
                  example: "$",
                },
                price: {
                  type: "number",
                  example: 53.11,
                },
                raw_text: {
                  type: "string",
                  example: "$53.11",
                },
              },
            },
            AmazonProductItem_ratings: {
              type: "object",
              properties: {
                rating_average: {
                  type: "number",
                  example: 4.8,
                },
                rating_total: {
                  type: "number",
                  example: 18498,
                },
                breakdown: {
                  $ref: "#/components/schemas/AmazonProductItem_ratings_breakdown",
                },
              },
            },
            AmazonProductItem_ratings_breakdown: {
              type: "array",
              items: {
                types: "object",
                properties: {
                  stars: {
                    type: "number",
                    example: 5,
                  },
                  percentage: {
                    type: "number",
                    example: 88,
                  },
                  count: {
                    type: "number",
                    example: 16278,
                  },
                },
              },
            },
            AmazonProductItem_information: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  title: {
                    type: "string",
                    example: "Safety Information",
                  },
                  text: {
                    type: "string",
                    example:
                      "Keep away from any source of flame. Pampers diapers like almost any article of clothing will burn if exposed to flame. To avoid risk of choking on plastic padding or other materials do not allow your child to tear the diaper or handle any loose pieces of the diaper. Discard any torn or unsealed diaper or any loose pieces of the diaper. To avoid suffocation keep all plastic bags away from babies and children. If you notice gel-like material on your baby's skin don't be alarmed. This comes from the diaper padding and can be easily removed by wiping your baby's skin with a soft dry cloth.",
                  },
                },
              },
            },
            AmazonProductItem_product_details: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  label: {
                    type: "string",
                    example: "Is Discontinued By Manufacturer",
                  },
                  value: {
                    type: "string",
                    example: "Yes",
                  },
                },
              },
            },
            AmazonProductItem_best_seller_ranks: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  rank: {
                    type: "number",
                    example: "344",
                  },
                  category: {
                    type: "string",
                    example: "Health & Household",
                  },
                  url: {
                    type: "string",
                    example:
                      "https://www.amazon.com/gp/bestsellers/hpc/ref=pd_zg_ts_hpc",
                  },
                },
              },
            },
            AmazonProductItem_amazon_choice: {
              type: "object",
              properties: {
                flag: {
                  type: "boolean",
                  example: true,
                },
                in: {
                  type: "string",
                  example: "Disposable Diapers by Pampers",
                },
              },
            },
            AmazonProductItem_feature_bullets: {
              type: "object",
              properties: {
                text: {
                  type: "array",
                  items: {
                    type: "string",
                    example: "Pampers is the #1 pediatrician recommended brand",
                  },
                },
                count: {
                  type: "number",
                  example: 8,
                },
              },
            },
            AmazonProductItem_frequently_bought_together_asins: {
              type: "array",
              items: {
                type: "string",
                example: "B005SPESKK",
              },
            },

            AmazonProductItem_aplus_content: {
              type: "object",
              properties: {
                available: {
                  type: "boolean",
                  example: true,
                },
                text: {
                  type: "string",
                  example: "hello text",
                },
                images: {
                  type: "array",
                  items: {
                    type: "string",
                    example:
                      "https://images-na.ssl-images-amazon.com/images/G/01/x-locale/common/grey-pixel.gif",
                  },
                },
              },
            },
          },
        },
      },
      400: {
        type: "object",
        properties: {
          success: {
            type: "boolean",
            example: false,
          },
          message: {
            type: "string",
          },
        },
      },
      503: {
        type: "object",
        properties: {
          success: {
            type: "boolean",
            example: false,
          },
          message: {
            type: "object",
            properties: {
              code: {
                type: "integer",
              },
              details: {
                type: "string",
              },
            },
          },
        },
      },
      404: {
        type: "object",
        properties: {
          success: {
            type: "boolean",
            example: false,
          },
          message: {
            type: "string",
          },
        },
      },
    },
  },
};
