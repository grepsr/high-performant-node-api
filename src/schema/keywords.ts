import { RouteShorthandOptions } from "fastify";

// require("ajv-errors")(ajv, { singleError: true });

export const AmazonKeywords: RouteShorthandOptions = {
  schema: {
    summary:
      "The Search API retrieves product search results from an Amazon Store.",
    description:
      "The Search API retrieves a list of products, as they appear on Amazon, including organic and sponsored results.\n",
    querystring: {
      type: "object",
      properties: {
        store: {
          type: "string",
          description:
            "Amazon domain to retrieve search results from for the search term provided. Supported list of Amazon domains can be found here.",
          style: "form",
          enum: ["US", "AE"],
        },
        search_term: {
          type: "string",
          description:
            "Search term to be used to retrieve product listing. Please note that the requested term and actual term may vary as Amazon may correct typos to return nearest matches",
          style: "form",
          explode: true,
        },
        page: {
          type: "integer",
          description:
            "Specific page number to retrieve the results from. Returns items from the first page if left empty.",
          style: "form",
          explode: true,
          minimum: 1,
        },
        category_id: {
          type: "string",
          description:
            "Provide a category_id if you want to limit the search results to a specific category only. If left empty, the API will search results across all categories.",
          style: "form",
          explode: true,
        },
        sort_by: {
          type: "string",
          style: "form",
          explode: true,
          enum: [
            "featured",
            "price_low_to_high",
            "price_high_to_low",
            "most_recent",
            "average_review",
          ],
          description:
            "Determines the order in which search results are returned from Amazon. Acceptable values are: featured, price_low_to_high, price_high_to_low, most_recent, average_review",
        },
        direct_search: {
          type: "boolean",
          enum: [true, false],
          style: "form",
          description:
            "Set direct_search=true if you do not want Amazon to auto correct your search term in case of a typo. If left empty, Amazon will look for nearest matches by automatically correcting the spelling in your search term",
          explode: true,
        },
        url: {
          type: "string",
          format: "url",
          description: "URL of the search page to be retrieved.",
        },
        include_html: {
          type: "boolean",
          description: "Set true, If you want to get html. Defaults to false",
        },
      },
      oneOf: [
        { required: ["search_term"], not: { required: ["url"] } },
        { required: ["url"], not: { required: ["search_term"] } },
      ],
      errorMessage: {
        properties: {
          sort_by:
            "querystring.sort_by should be equal to one of the allowed values, Acceptable values are: featured, price_low_to_high, price_high_to_low, most_recent, average_review",
        },
      },
    },
    response: {
      200: {
        type: "object",
        description: "Product Details matching the parameters",
        properties: {
          success: {
            type: "boolean",
          },
          request_parameters: {
            type: "object",
            properties: {
              store: {
                type: "string",
                example: "US",
              },
              // domain: {
              //   type: "string",
              // },
              search_term: {
                type: "string",
              },
              page: {
                type: "integer",
              },
              category_id: {
                type: "string",
              },
              sort_by: {
                type: "string",
              },
              direct_search: {
                type: "boolean",
              },
              url: {
                type: "string",
              },
              include_html: {
                type: "boolean",
              },
            },
          },
          request_details: {
            type: "object",
            properties: {
              requested_url: {
                type: "string",
              },
              canonical_url: {
                type: "string",
              },
              created_at: {
                type: "string",
              },
              response_ms: {
                type: "number",
              },
            },
          },
          content: { $ref: "#/definitions/message" },
        },
        definitions: {
          message: {
            type: "object",
            properties: {
              search_term_requested: {
                type: "string",
                example: "phone",
              },
              search_term_actual: {
                type: "string",
                example: "phone",
              },
              search_results: {
                $ref: "#/components/schemas/AmazonSearch_results",
              },
              filters: {
                $ref: "#/components/schemas/AmazonSearch_filters",
              },
              related_brands: {
                $ref: "#/components/schemas/AmazonSearch_related_brands",
              },
              related_searches: {
                $ref: "#/components/schemas/AmazonSearch_related_searches",
              },
              editorial_recommendation: {
                $ref: "#/components/schemas/AmazonSearch_editorial_recommendation",
              },
              pagination: {
                $ref: "#/components/schemas/AmazonSearch_pagination",
              },
              html: {
                type: "string",
                example: '"<!DOCTYPE html><html lang="en-us" .....',
              },
            },
            required: ["search_term_actual"],
          },
        },
        components: {
          schemas: {
            AmazonSearch_filters: {
              type: "object",
              additionalProperties: true,
            },
            AmazonSearch_pagination: {
              type: "object",
              properties: {
                organic_results_per_page: {
                  type: "number",
                  example: 1,
                },
                results_per_page: {
                  type: "integer",
                },
                results_position_start: {
                  type: "number",
                  example: 1,
                },
                results_position_end: {
                  type: "number",
                  example: 48,
                },
                results_total: {
                  type: "integer",
                },
                current_page: {
                  type: "integer",
                },
                available_pages: {
                  type: "integer",
                },
                next_page_url: {
                  type: "string",
                },
              },
            },
            AmazonSearch_results: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  rank_absolute: {
                    type: "number",
                    example: 1,
                  },
                  rank_relative_to_listing_type: {
                    type: "number",
                    example: 1,
                  },
                  asin: {
                    type: "string",
                    example: "B07LFQ8TW3",
                  },
                  platform: {
                    type: "string",
                    example: "Amazon Prime",
                  },
                  product_title: {
                    type: "string",
                    example: "Starbucks Flavored Ground Coffee",
                  },
                  brand: {
                    type: "string",
                    example: "Starbucks",
                  },
                  price: {
                    $ref: "#/components/schemas/AmazonProductItem_price",
                  },
                  image_url: {
                    type: "string",
                    example:
                      "https://m.media-amazon.com/images/I/818xmNKFzfL._AC_SR180,120_QL70_.jpg",
                  },
                  video_url: {
                    type: "string",
                    example:
                      "https://d1f0esyb34c1g2.cloudfront.net/transcode/storyTeller/ENTITY1D11ZDXTODDKJ/6e0b7314-40fd-40f1-9b3b-1136aff48420-SBV_291884-T1/fc47898d-e02d-4f55-b7ad-28732c69064b/1601633956522sbv.mp4",
                  },
                  product_url: {
                    type: "string",
                    example:
                      "https://aax-us-iad.amazon.com/x/c/Qm17-vhCdQqVt1aFhzy9tvgAAAF-Pg9PogEAAAH2AdVMD1I/https://www.amazon.com/gp/aw/d/B07LFQ8TW3/?_encoding=UTF8&pd_rd_plhdr=t&aaxitk=566212805cabe36b821df4278a78faa9&hsa_cr_id=2135345400201&ref_=sbx_be_s_sparkle_mcd_asin_0_bkgd&pd_rd_w=oTlGU&pf_rd_p=488a18be-6d86-4de0-8607-bd4ea4b560f3&pd_rd_wg=dUTGn&pf_rd_r=DZ6QR92V1PX7RC0SKW0G&pd_rd_r=85177ba2-a426-4b96-acea-99055bb255cb",
                  },
                  listing_type: {
                    type: "string",
                    example: "Sponsored",
                  },
                  ratings_average: {
                    type: "number",
                    example: 2.2,
                  },
                  ratings_total: {
                    type: "number",
                    example: 21,
                  },
                  bestseller: {
                    $ref: "#/components/schemas/AmazonProductItem_bestseller",
                  },
                  climate_pledge_friendly: {
                    $ref: "#/components/schemas/AmazonProductItem_climatePledgeFriendly",
                  },
                  coupon: {
                    $ref: "#/components/schemas/AmazonSearch_coupon",
                  },
                  flag_amazon_choice: {
                    type: "boolean",
                    example: false,
                  },
                  subscribe_and_save: {
                    $ref: "#/components/schemas/AmazonProductItem_price",
                  },
                  brand_share_page_percentage: {
                    type: "number",
                  },
                  brand_share_listing_type_percentage: {
                    type: "number",
                  },
                },
              },
            },
            AmazonProductItem_bestseller: {
              type: "object",
              properties: {
                is_bestseller: {
                  type: "boolean",
                  example: true,
                },
                category: {
                  type: "string",
                  example: "in Apparel & Accessories",
                },
                url: {
                  type: "string",
                  example:
                    "https://www.amazon.com/gp/bestsellers/apparel-accessories/ref=pd_zg_hrsr_hd_b_1_1_1?ie=UTF8&pg=1&dc&fst=as%3Aoff&qid=1558242712&refinements=p_n_feature_twelve_browse-bin%3A1250732011",
                },
              },
            },
            AmazonProductItem_climatePledgeFriendly: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  text: {
                    type: "string",
                    example: "Hello organic",
                  },
                  image: {
                    type: "string",
                    example:
                      "https://m.media-amazon.com/images/I/31VxmWkps7L._SS200_.png",
                  },
                  url: {
                    type: "string",
                    example:
                      "https://www.amazon.com/gp/slredirect/picassoRedirect.html/ref=pa_sp_btf_aps_sr_pg1_1?ie=UTF8&adId=A01901641FAI8YX7V6V53&url=%2FCeylon-Cinnamon-Certified-Supplement-Capsules%2Fdp%2FB00BYM6YW6%2Fref%3Dsr_1_56_sspa%3Fkeywords%3Dcinnamon%2Bcapsule%26qid%3D1650455367%26sr%3D8-56-spons%26psc%3D1&qualifier=1650455367&id=8985171302934152&widgetName=sp_btf",
                  },
                },
              },
            },
            AmazonSearch_coupon: {
              type: "object",
              properties: {
                badge: {
                  type: "string",
                },
                text: {
                  type: "string",
                },
              },
            },
            AmazonSearch_editorial_recommendation: {
              type: "object",
              properties: {
                review_title: {
                  type: "string",
                  example: "Best Coffee",
                },
                review_description: {
                  type: "string",
                  example:
                    "Coffee is loved around the world for its delicious taste and its ability to boost energy. A few people can start their day without a delicious cup, but no matter the time of day, the quality of your coffee is incredibly important. There are thousands of options on the market; some are rich and strong, while others are delicate and smooth. To make the decision easy, we compiled a buying guide with the best options available. Taking into account flavor, origin, and convenience, we rounded up the best coffee K-cups for you to pick your new favorite.",
                },
                review_date: {
                  type: "string",
                  example: "2021-12-20",
                },
                review_url: {
                  type: "string",
                  example:
                    "https://www.amazon.com/ospublishing/onsite-associates/info?linkCode=oas&asc_contentid=amzn1.osa.710e4e63-8bfd-4e86-b21e-b6b7a02b6060.ATVPDKIKX0DER.en_US&cv_ct_id=amzn1.osa.710e4e63-8bfd-4e86-b21e-b6b7a02b6060.ATVPDKIKX0DER.en_US&asc_contenttype=article&cv_ct_pg=search&cv_ct_wn=osp-single-source-earns-comm&cv_ct_we=disclaimer",
                },
                review_author: {
                  type: "string",
                  example: "Clean Eating",
                },
                review_author_url: {
                  type: "string",
                  example:
                    "https://www.amazon.com/gp/profile/amzn1.account.AFIZUYSBP3FZ3ZOG5W7WDWM52VLQ/ref=sxin_14?linkCode=oas&asc_contentid=amzn1.osa.710e4e63-8bfd-4e86-b21e-b6b7a02b6060.ATVPDKIKX0DER.en_US&cv_ct_id=amzn1.osa.710e4e63-8bfd-4e86-b21e-b6b7a02b6060.ATVPDKIKX0DER.en_US&asc_contenttype=article&cv_ct_pg=search&cv_ct_wn=osp-single-source-earns-comm&cv_ct_we=contributor-profile&pd_rd_w=UYIlA&pf_rd_p=5846ecd6-3f37-4a28-8efc-9c817c03dbe9&pf_rd_r=DZ6QR92V1PX7RC0SKW0G&pd_rd_r=aa0ba8be-b4e3-413b-af24-1578883f44b9&pd_rd_wg=5aSdA&qid=1641718697&cv_ct_cx=coffee",
                },
              },
            },
            AmazonSearch_related_searches: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  name: {
                    type: "string",
                    example: "ground coffee",
                  },
                  url: {
                    type: "string",
                    example:
                      "https://www.amazon.com/s/?k=ground+coffee&ref=sugsr_0_10&pd_rd_w=wKMUY&pf_rd_p=4fa0e97a-13a4-491b-a127-133a554b4da3&pf_rd_r=DZ6QR92V1PX7RC0SKW0G&pd_rd_r=9659be60-79bb-4c31-be39-cf48d74d611e&pd_rd_wg=daT7d&qid=1641718697",
                  },
                },
              },
            },
            AmazonSearch_related_brands: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  brand_name: {
                    type: "string",
                    example: "ground coffee",
                  },
                  brand_logo_url: {
                    type: "string",
                    example:
                      "https://m.media-amazon.com/images/S/al-na-9d5791cf-3faf/412f8230-a284-4441-8655-c32e3013c5e7._CR0,0,400,400_AC_SX139_SY100_QL70_.jpg",
                  },
                  brand_byline: {
                    type: "string",
                    example: "Find your favorite Starbucks® coffee.",
                  },
                  brand_image_url: {
                    type: "string",
                    example:
                      "https://m.media-amazon.com/images/I/818xmNKFzfL._AC_SR139,139_QL70_.jpg",
                  },
                  url: {
                    type: "string",
                    example:
                      "https://aax-us-iad.amazon.com/x/c/QmaCdC_azGa_Smd-t77oEmoAAAF-Pg9QgQEAAAH2AWo5i14/https://www.amazon.com/stores/StarbucksCoffee/GroundWholeBean/page/9DE86EBD-AC89-4817-A85B-BD85E2820E92/?_encoding=UTF8&store_ref=SB_A0976793WUZ74791CB3C&pd_rd_plhdr=t&aaxitk=73dde3f0bd783e25709c1ee17131d1aa&hsa_cr_id=5224149960001&lp_asins=B07LFQ8TW3%2CB07LFSTZSB%2CB08ZWK9Q6S&lp_query=coffee&lp_slot=desktop-hsa-3psl&ref_=sbx_be_s_3psl_mbd_bkgd&pd_rd_w=1eUcx&pf_rd_p=85014337-b4b1-4f3c-85f8-828d3b814280&pd_rd_wg=XEaW0&pf_rd_r=DZ6QR92V1PX7RC0SKW0G&pd_rd_r=a78459d1-4c7a-4c09-a361-481f95a9490d",
                  },
                },
              },
            },
            AmazonProductItem_price: {
              type: "object",
              properties: {
                currency_name: {
                  type: "string",
                  example: "USD",
                },
                currency_symbol: {
                  type: "string",
                  example: "$",
                },
                price: {
                  type: "number",
                  example: 53.11,
                },
                price_unit: {
                  type: "string",
                  example: "Count",
                },
                price_per_unit: {
                  type: "number",
                  example: 0.21,
                },
                raw_text: {
                  type: "string",
                  example: "$53.11 ($0.21 / Count)",
                },
              },
            },
          },
        },
      },
      400: {
        type: "object",
        properties: {
          success: {
            type: "boolean",
            example: false,
          },
          message: {
            type: "string",
          },
        },
      },
      503: {
        type: "object",
        properties: {
          success: {
            type: "boolean",
            example: false,
          },
          message: {
            type: "object",
            properties: {
              code: {
                type: "integer",
              },
              details: {
                type: "string",
              },
            },
          },
        },
      },
    },
  },
};
