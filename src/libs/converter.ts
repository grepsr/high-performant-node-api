// export const grpcResponseRenameKeys = (obj: Object) => Object.fromEntries(
//     Object.entries(obj)
//         .map(([key, val]) => [
//             processVal(val)
//         ])
// );

// const processVal = val => (

//     typeof val !== 'object'
//         ? checkFloatingValue(val)
//         : Array.isArray(val)
//             ? val.map(grpcResponseRenameKeys)
//             : grpcResponseRenameKeys(val)

// );

export const checkFloatingValue = val => {
   return  isNaN(val) ? val : parseFloat(val).toFixed(2);
};