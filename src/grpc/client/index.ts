import grpc from 'grpc';
import { loadSync } from '@grpc/proto-loader';
import fs from 'fs';
import path from 'path';
import { cfg } from '../../utils/config';

const INSECURE_ADDRESS = 'localhost:50050';
const SECURE_ADDRESS = 'envoy-proxy.frontend.grepsr.marathon.mesos:4443';
const PROXY_CERT_PATH = '/home/samrat/certs/envoy-proxy.cert';
//`${cfg('ca_cert.path', '/certs/envoy-proxy.cert')}`;

const APP_ENV = process.env.APP_ENV || 'dev';

const loadProtoFile = (protoFileName: String) => {
  let fileName = path.dirname(__filename) + `/../../../protos/${protoFileName}.proto`;
  if(APP_ENV == 'prd'){
    fileName = path.dirname(__filename) + `/../../../high-performant-node-api/protos/${protoFileName}.proto`;
  }
  const packageDefinition = loadSync(fileName, { keepCase: true, defaults: true});
  const protoDescriptor = grpc.loadPackageDefinition(packageDefinition);
  return protoDescriptor;
};

const getInterceptor = (name: string) => {
  const interceptor = (options, nextCall) => {
    const requester = {
      start: (metadata, listener, next) => {
        metadata.add('x-service-name', name);
        metadata.add(
          'x-caller',
          process.env.SERVICE_NAME || 'phoenix-events-processor'
        );
        next(metadata, listener);
      }
    };
    return new grpc.InterceptingCall(nextCall(options), requester);
  };
  return interceptor;
};

const getVersionedClusterName = (clusterName: string): string => {
  let versionedClusterName: string = clusterName;
  try {
    const serviceVersion: string = cfg(`service_versions.${clusterName}`);
    if (serviceVersion && serviceVersion !== 'default') {
      versionedClusterName = serviceVersion;
    }
  } catch (err) {
    console.log(err);
    console.error('Could not find version in config file, using default.');
  }
  return versionedClusterName;
};

/**
 * getClient will do all the hard work of connecting to the cluster, initializing
 * an instance for the desired service, adding the interceptors etc. and
 * return it to the caller.
 * @param {string} clusterName - Name of the cluster in envoy
 * @param {string} protoFileName - Filename of the service inside the protos folder
 * @param {string} service - Name of the service inside the protos file
 * @param {boolean} secure - Whether to create a secure channel or not
 */

interface Descriptor {
  grepsr: Object,
  grepsrnext: Object
}

const getClient = (
  clusterName: string,
  protoFileName: string,
  service: string,
  secure: boolean = false,
  version: any = null
) => {
  let client = null;
  const protoDescriptor: any = loadProtoFile(protoFileName);

  const versionedClusterName = getVersionedClusterName(clusterName);

  // We are assuming that the package name of all
  // proto files will always be "grepsr".
  const { grepsr, grepsrnext }: Descriptor = protoDescriptor;

  // Kubernetes.
  // This env. variable will be present in Kubernetes server only.
  const isKubernetes = process.env.KUBERNETES_SERVICE_HOST;
  if (isKubernetes) {
    let host = versionedClusterName;
    if (version) {
      host = `${host}-v${version}`;
    }

    const domainName = `${cfg(
      'infra.domain',
      'grepsr.net'
    )}`;
    host = `${host}.${domainName}`;

    // Create a secure channel by default.
    let credentials: any = null;
    try {
      credentials = grpc.credentials.createSsl(
        fs.readFileSync('/etc/ssl/certs/ca-certificates.crt')
      );
    } catch (e) {
      console.log('Could not load ca-certificates');
      // Local (strictly for testing on Mac only)
      credentials = grpc.credentials.createSsl(
        fs.readFileSync('/usr/local/etc/openssl@1.1/cert.pem')
      );
    }
    if (protoFileName === 'aclnext') {
      client = new grepsrnext[service](host, credentials, {});
    } else {
      client = new grepsr[service](host, credentials, {});
    }
    return client;
  }

  // DCOS.

  if (secure) {
    // Create a secure client
    const interceptor = getInterceptor(versionedClusterName);
    let credentials: any = null;
    try {
      credentials = grpc.credentials.createSsl(
        fs.readFileSync(PROXY_CERT_PATH)
      );
    } catch (e) {
      credentials = grpc.credentials.createSsl(
        fs.readFileSync(path.dirname(__filename) + '/../../../certificates/envoy-proxy.cert')
      );
    }
    if (protoFileName === 'aclnext') {
      client = new grepsrnext[service](SECURE_ADDRESS, credentials, {
        interceptors: [interceptor],
        "grpc.max_receive_message_length": 1024 * 1024 * 100,
        "grpc.max_send_message_length": 1024 * 1024 * 100
      });
    } else {
      client = new grepsr[service](SECURE_ADDRESS, credentials, {
        interceptors: [interceptor],
        "grpc.max_receive_message_length": 1024 * 1024 * 100,
        "grpc.max_send_message_length": 1024 * 1024 * 100
      });
    }
  } else {
    client = new grepsr[service](
      INSECURE_ADDRESS,
      grpc.credentials.createInsecure(),
      {"grpc.max_receive_message_length": 1024 * 1024 * 100,"grpc.max_send_message_length": 1024 * 1024 * 100}
    );
  }

  return client;
};

export default getClient;
