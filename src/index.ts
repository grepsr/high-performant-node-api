import Fastify from 'fastify';
import routes from './routes';
import fastifySwagger from 'fastify-swagger';
import getClient from "./grpc/client/";
import fastifyCors from 'fastify-cors';


const fastify = Fastify({});
fastify.register(fastifyCors, { origin: '*' });


fastify.register(fastifySwagger, {
  routePrefix: '/documentation',
  swagger: {
    info: {
      title: 'E-commerce Insights API',
      description: 'API for different types of e-commerce data from multiple retailers across the world.',
      version: '1.0.0',
      contact: {
        email: "api@grepsr.com"
      }
    },
    schemes: ['http'],
    consumes: ['application/json'],
    produces: ['application/json'],
    tags: [
      { name: 'user', description: 'User related end-points' },
      { name: 'code', description: 'Code related end-points' }
    ],
    definitions: {},
    securityDefinitions: {
      apiKey: {
        type: 'apiKey',
        name: 'apiKey',
        in: 'header'
      }
    }
  },
  uiConfig: {
    docExpansion: 'full',
    deepLinking: false
  },
  uiHooks: {
    onRequest: function (request, reply, next) { next() },
    preHandler: function (request, reply, next) { next() }
  },
  staticCSP: true,
  transformStaticCSP: (header) => header,
  exposeRoute: true
})

const amazon_client: any = getClient(
  "phoenix-scraper-service",
  "scraper",
  "AmazonService",
  false
);

const parse_html_client: any = getClient(
  "phoenix-scraper-service",
  "scraper",
  "ScraperService",
  false
);

fastify.register(routes, { prefix: '/v1', amazon_client, parse_html_client });
console.log(`APP PORT: ${process.env.APP_PORT}`);
const port: number = +process.env.APP_PORT! || 3001;

const init = async (): Promise<void> => {
  try {
    fastify.listen(port, '0.0.0.0', () => {
      console.log(`Server up on ${port}`);
    });
  } catch (error) {
    console.log(error);
    console.log("dsad");
    fastify.log.error(error);
    process.exit(1);
  }
};

init();
