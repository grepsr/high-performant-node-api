import { FastifyInstance, RouteShorthandOptions, FastifySchema } from "fastify";
//import Ajv from "ajv";
// import ajvErrors from "ajv-errors";
import { AmazonProducts } from "../schema/products";
import {
  testRequest,
  ping,
  appSearchKeywords,
  appSearchProducts,
} from "../controllers/tests";
import { getKeywordDetails } from "../controllers/keywords";
import { getProductDetails } from "../controllers/product";
import { AmazonKeywords } from "../schema/keywords";
import { AmazonReviews } from "../schema/reviews";
import { getReviewsDetails } from "../controllers/reviews";
import { AmazonOffers } from "../schema/offers";
import { getOffersDetails } from "../controllers/offers";
import { getSellerDetails } from "../controllers/sellers";
import { AmazonSellers } from "../schema/sellers";
import { AmazonBestSellers } from "../schema/bestsellers";
import { getBestSellersDetails } from "../controllers/bestselers";
import { getParseBody } from "../controllers/parse";
import { getCategories } from "../controllers/categories";

// const ajv = new Ajv({ allErrors: true })
// require('ajv-errors')(ajv);
// const validate = ajv.validateSchema(AmazonKeywords);
// const AmazonSearchSchema = ajvErrors(validate);

const routes = (fastify: FastifyInstance, options, done): void => {
  const { amazon_client, parse_html_client } = options;
  fastify.get("/", testRequest);
  fastify.get("/ping", ping);
  fastify.get("/amazon/product", AmazonProducts, getProductDetails(amazon_client));
  fastify.get("/amazon/search", AmazonKeywords, getKeywordDetails(amazon_client));
  fastify.get("/amazon/reviews", AmazonReviews, getReviewsDetails(amazon_client));
  fastify.get("/amazon/offers",AmazonOffers,getOffersDetails(amazon_client));
  fastify.get("/amazon/sellers",AmazonSellers,getSellerDetails(amazon_client));
  fastify.get("/amazon/bestsellers",AmazonBestSellers,getBestSellersDetails(amazon_client));
  fastify.get("/categories", getCategories(parse_html_client  ));
  fastify.get("/parse/body", getParseBody(parse_html_client));
  // fastify.get("/search", appSearchKeywords);
  // fastify.get("/product", appSearchProducts);

  done();
};

export default routes;
