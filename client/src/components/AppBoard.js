import React, { Component } from "react";
import SearchBoard from "./SearchBoard";
import axios from "axios";
import ViewerBoard from "./ViewerBoard";

class AppBoard extends Component {
  constructor() {
    super();
    this.state = {
      mainLoader: false,
      searchInput: "",
      category: "product",
      processCount: "1/8",
      suggestionCount: "",
      info: {},
    };
    this.onChange = this.onChange.bind(this);
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  onUpdateHandler = (value) => {
    this.setState(value);
  };

  handleSearch = () => {
    const { searchInput, category } = this.state;
    this.setState({ mainLoader: true });

    let url = `/v1/search?search_term=${searchInput}`;
    if (category === "product") {
      url = `/v1/product?asin=${searchInput}`;
    }
    //url = "https://jsonplaceholder.typicode.com/todos";
    //console.log("Search Submitted: " + searchInput);

    axios
      .get(url)
      .then((res) => {
        this.setState({ mainLoader: false, info: res.data });

        //axios.get(`status?id=${res.data.id}`).then((resStat) => {
        // let processStat = resStat.data.message;
        // let processCount = resStat.data.processing_status;
        // if (processStat === "complete") {
        //   console.log("Status Completed");
        //   this.setState({
        //     mainLoader: false,
        //     processCount: "1/8",
        //     tabsData: resStat.data.data,
        //     suggestionCount: resStat.data.suggestions_count,
        //   });
        // } else {
        //   console.log("Still Need Processing");
        //   this.setState({
        //     processCount,
        //     tabsData: resStat.data.data,
        //     suggestionCount: resStat.data.suggestions_count,
        //   });
        //   setTimeout(() => {
        //     this.handleSearch();
        //   }, 5000);
        // }
        //});
      })
      .catch((e) =>
        this.setState({ mainLoader: false, info: e.response.data })
      );
  };

  render() {
    const { info } = this.state;
    return (
      <div className="container">
        <SearchBoard
          data={this.state}
          onUpdateHandler={this.onUpdateHandler}
          handleSearch={this.handleSearch}
        />
        {Object.keys(info).length > 0 ? <ViewerBoard data={info} /> : ""}
      </div>
    );
  }
}

export default AppBoard;
