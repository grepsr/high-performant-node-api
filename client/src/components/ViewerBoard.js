import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import ReactJson from 'react-json-view'



const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    marginTop: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(1),
    color: theme.palette.text.secondary,
  },
}));

// const setHtmlText = (val) => (
//   <text dangerouslySetInnerHTML={{ __html: val }} is="x3d"></text>
// );

export default function ViewerBoard(props) {
  const classes = useStyles();
//   const [value, setValue] = React.useState("1");
  const data = props.data || {};
//   console.log(data);

  return (
    <div className={classes.root}>
     <ReactJson src={data} />
    </div>
  );
}
