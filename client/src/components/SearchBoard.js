import React from "react";
import SearchBar from "material-ui-search-bar";
import Typography from "@material-ui/core/Typography";
import { CircularProgress } from "@material-ui/core";
import {
  FormControl,
  InputLabel,
  MenuItem,
  Box,
  Select,
} from "@material-ui/core";
// import Select, { SelectChangeEvent } from '@mui/material/Select';

export default function SearchBoard(props) {
  // const [progress, setProgress] = React.useState(0);
  // React.useEffect(() => {
  //   setProgress((prevProgress) =>
  //     (props.data.processCount === "1/8") ? 13 :
  //       prevProgress >= 100 ? 0 : prevProgress + 13.2
  //   );
  // }, [props.data.processCount]);

  const handleChange = (newValue) => {
    props.onUpdateHandler({ searchInput: newValue });
  };

  const handleSelectChange = (event) => {
    props.onUpdateHandler({ category: event.target.value });
  };

  function ProcessingStat() {
    return (
      <div>
        <div className="d-flex mt-3 justify-content-center">
          <CircularProgress size={25} />
          <Typography
            variant="h6"
            style={{ marginLeft: "15px", fontWeight: "800" }}
          >
            Getting Amazon Data
          </Typography>
        </div>
      </div>
    );
  }
  return (
    <div>
      <div className="row">
        <div className="col-12 text-center mt-4">
          <Typography variant="h4" style={{ fontWeight: "800" }}>
            Helix API Calls
          </Typography>
        </div>
      </div>
      <br />
      <div className="row">
        <div className="col-12">
          <div className="d-flex">
            <div className="col-3">
              <Box sx={{ minWidth: 120 }}>
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">
                    Category
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={props.data.category}
                    label="Category"
                    onChange={handleSelectChange}
                  >
                    <MenuItem value="product">Product</MenuItem>
                    <MenuItem value="search">Search</MenuItem>
                  </Select>
                </FormControl>
              </Box>
            </div>
            <div className="col-9">
              <SearchBar
                placeholder={
                  props.data.category === "product"
                    ? "Type Amazon Asin to search"
                    : "Search Keyword.."
                }
                onChange={(newValue) => handleChange(newValue)}
                value={props.data.searchInput}
                disabled={props.data.mainLoader}
                onRequestSearch={props.handleSearch}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          {props.data.mainLoader ? <ProcessingStat /> : ""}
        </div>
      </div>
    </div>
  );
}
