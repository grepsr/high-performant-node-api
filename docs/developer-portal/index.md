---
title: Ecommerce API Training program
---

# Training program

Helix API is an API to retrieve data from any Amazon domain worldwide in real-time. Getting Amazon data with Helix API is as simple as making an HTTP GET request to the request endpoint. The only required parameters are api_key(sign up for free to get an API key) and type (which defines the type of Amazon data you'd like to retrieve).
Spend between an hour and a day playing with the most commonly utilized features.

## Prerequisites
- Get API KEY

## Asking for help

Don't be shy... we're here to help.