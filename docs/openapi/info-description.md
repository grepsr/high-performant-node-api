
# Getting Started

A comprehensive documentation of **Grepsr’s Retail Data & Analytics API** including code snippets and tutorials to use the different API endpoints. 

# About the API

Grepsr’s Retail and Analytics API is the easiest way for businesses to get real-time product data from Amazon. Our dedicated REST APIs for Amazon products, search, bestsellers, and reviews allows you to access high quality, complete, and accurate data on-demand reliably and at scale. We use residential proxies to emulate local traffic to provide you with the most comprehensive dataset. Our simple metered subscription model and transparent pricing allows you to scale up or down without any long term commitment. 

## Get your API key

Reach out to us at sales@gohelix.xyz for an API key. Someone from our team will reach out to you with the API key to your account. Your requests will be validated using the unique API key for your account. The API key remains valid indefinitely and does not expire. Since the API key is your personalized identification key, and therefore highly sensitive, it is recommended that you save this information as a variable and simply use the variable while executing the requests. Also, do not publicly share your API key in code repositories, client-side code, etc. 
 
If you come across a variable ``{{YOUR_API_KEY_HERE}}`` in this documentation, please note that it refers to your personal API key.

## Versioning

The current version of the API is v1. Please use https://api.gohelix.xyz/v1 as the URL prefix for each request. When a newer version is released, you can refer to our API changelog to learn about backward compatibility.
